package com.example.davalebateqvsmeti.room.Dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.davalebateqvsmeti.room.Entity.newsEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NewsDao {
    @Query("SELECT * FROM news")
    fun getAll(): Flow<List<newsEntity>>

    @Insert
    fun insertAll(vararg items: newsEntity)

    @Delete
    fun delete(items: newsEntity)

    @Query("DELETE FROM news")
    fun delAll()
}