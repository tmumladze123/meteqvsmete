package com.example.davalebateqvsmeti.Fragments

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import com.example.davalebateqvsmeti.databinding.FragmentAddNewsBinding
import com.example.davalebateqvsmeti.newsViewModel

class addNewsFragment : BaseFragment<FragmentAddNewsBinding>(FragmentAddNewsBinding::inflate) {
    private val newsModel: newsViewModel by viewModels()
    override fun start() {
       // listeners()
    }
    fun listeners()
    {
        binding.btnAdd.setOnClickListener {
            if(binding.etDesc.text!!.isNotEmpty() && binding.etLink.text!!.isNotEmpty()
                && binding.etTitle.text!!.isNotEmpty())
            {
                newsModel.addNews(binding.etTitle.text.toString(),binding.etDesc.text.toString(), binding.etLink.text.toString())
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            }
        }
    }
}