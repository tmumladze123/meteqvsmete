package com.example.davalebateqvsmeti.room.DataBase

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.davalebateqvsmeti.contextClass
import com.example.davalebateqvsmeti.room.Dao.NewsDao
import com.example.davalebateqvsmeti.room.Entity.newsEntity


@Database(entities = [newsEntity::class], version = 1)
abstract class newsDataBase : RoomDatabase() {
    abstract fun shoppingDao(): NewsDao
    companion object {
        val db = Room.databaseBuilder(
            contextClass.context!!,
            newsDataBase::class.java, "database-name"
        ).build()
    }
}