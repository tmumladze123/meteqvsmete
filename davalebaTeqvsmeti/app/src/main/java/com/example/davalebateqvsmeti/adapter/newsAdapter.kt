package com.example.davalebateqvsmeti.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.davalebateqvsmeti.Extensions.setPhoto
import com.example.davalebateqvsmeti.databinding.NewsItemBinding
import com.example.davalebateqvsmeti.room.Entity.newsEntity

class newsAdapter : RecyclerView.Adapter<newsAdapter.ViewHolder>() {
    var news= mutableListOf<newsEntity>()
    inner class ViewHolder(val binding: NewsItemBinding) : RecyclerView.ViewHolder(binding.root) {
        private lateinit var Item: newsEntity
        fun bind() {
            Item = news[adapterPosition]
            binding.apply {
                tvTitle.text = Item.title
                tvDesc.text = Item.desc.toString()
                ivNewsItem.setPhoto(Item.picture)
            }
        }
    }
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): newsAdapter.ViewHolder =
            ViewHolder(NewsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

        override fun onBindViewHolder(holder: newsAdapter.ViewHolder, position: Int) {
            holder.bind()
        }

        override fun getItemCount(): Int = news.size
        fun setData(news: ArrayList<newsEntity>) {
            this.news.clear()
            this.news.addAll(news)
            notifyDataSetChanged()
        }

    }