package com.example.davalebateqvsmeti.Fragments

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.davalebateqvsmeti.R
import com.example.davalebateqvsmeti.adapter.newsAdapter
import com.example.davalebateqvsmeti.databinding.FragmentNewsBinding
import com.example.davalebateqvsmeti.newsViewModel
import com.example.davalebateqvsmeti.room.Entity.newsEntity
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class newsFragment : BaseFragment<FragmentNewsBinding>(FragmentNewsBinding::inflate) {
    private val newsModel: newsViewModel by viewModels()
    var adapter=newsAdapter()
    override fun start() {
        initAdapter()
        observers()
        listeners()
    }
    fun listeners()
    {
        binding.btnAdd.setOnClickListener {
            findNavController().navigate(R.id.action_newsFragment_to_addNewsFragment)
        }
    }

    fun initAdapter()
    {
        binding.rvNews.adapter=adapter
        binding.rvNews.layoutManager= GridLayoutManager(context, 2)
    }
    fun observers() {
        viewLifecycleOwner.lifecycleScope.launch {
            newsModel.roomFlow?.collect {
                var arr=ArrayList<newsEntity>()
                for(element in it)
                    arr.add(newsEntity(element.title!!,element.desc!!,element.picture!!))
                adapter.setData(arr)
            }
        }
    }
}