package com.example.davalebateqvsmeti.room.Entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="news")
data class newsEntity(
    @PrimaryKey(autoGenerate = true) val uid: Int=0,
    @ColumnInfo(name = "title") val title: String?,
    @ColumnInfo(name = "desc") val desc: String?,
    @ColumnInfo(name = "picture") val picture: String?
){

    constructor(title: String?, desc: String?, picture: String?) : this(0, title, desc,picture)
}
