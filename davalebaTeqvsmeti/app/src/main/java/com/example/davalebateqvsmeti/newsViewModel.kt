package com.example.davalebateqvsmeti

import androidx.lifecycle.ViewModel
import com.example.davalebateqvsmeti.room.DataBase.newsDataBase
import com.example.davalebateqvsmeti.room.Entity.newsEntity
import kotlinx.coroutines.flow.Flow

class newsViewModel : ViewModel() {
    var roomFlow: Flow<List<newsEntity>>? = newsDataBase.db.shoppingDao().getAll()
    fun addNews(title:String,desc:String,url:String)
    {
        newsDataBase.db.shoppingDao().insertAll(
            newsEntity(title,desc,url)
        )
    }
}